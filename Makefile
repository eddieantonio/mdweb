include mdweb.inc

PACKAGE = mdweb
VERSION = 0.2

MDWEB_SOURCE = mdweb.nw
MAIN_EXECUTABLE = mdweb.py
MAIN_DOCUMENTATION = README.text

DIST_NAME = $(PACKAGE)-$(VERSION)
DIST_INCLUDE = $(MDWEB_SOURCE) $(MAIN_EXECUTABLE) $(MAIN_DOCUMENTATION) \
			   Makefile TODO
BASEDIR = /usr/local
BINDIR = $(BASEDIR)/bin
MANDIR = $(BASEDIR)/share/man
MAN1DIR = $(MANDIR)/man1

all: $(MAIN_EXECUTABLE) $(MAIN_DOCUMENTATION)

$(MAIN_EXECUTABLE): $(MDWEB_SOURCE)
	$(MDTANGLE) -R$@ $(MDWEB_SOURCE) > $@
	chmod u+x $(MAIN_EXECUTABLE)
$(MAIN_DOCUMENTATION): $(MAIN_EXECUTABLE) $(MDWEB_SOURCE)
	$(MDWEAVE) $(MDWEB_SOURCE) > $@

dist: $(MAIN_EXECUTABLE) $(MAIN_DOCUMENTATION)
	mkdir $(DIST_NAME)
	cp $(DIST_INCLUDE) $(DIST_NAME)/
	tar czf $(DIST_NAME).tar.gz $(DIST_NAME)
	$(RM) -r $(DIST_NAME)/

doc: $(MAIN_DOCUMENTATION)

_clean:
	$(RM) $(MAIN_EXECUTABLE) $(MAIN_DOCUMENTATION)

install: $(MAIN_EXECUTABLE)
	cp $< $(BINDIR)/$(PACKAGE)

uninstall:
	rm $(BINDIR)/${MAIN_EXECUTABLE:.py=} $(MAN1DIR)/$(PACKAGE).1

TEST_DIR = test
TEST_VERSION = 20120713
TEST_FILE = $(TEST_DIR)/mdweb-$(TEST_VERSION).nw
TEST_WEAVE = $(TEST_DIR)/README-$(TEST_VERSION).text.ref
TEST_CHUNK = mdweb.py
TEST_TANGLE = $(TEST_DIR)/mdweb-$(TEST_VERSION).py.ref

test: $(MAIN_EXECUTABLE)
	@chmod u+x $<
	@./$< --weave $(TEST_FILE) | diff -u $(TEST_WEAVE) -
	@./$< -R $(TEST_CHUNK) $(TEST_FILE) | diff -u $(TEST_TANGLE) -
	@echo '$<' passed all tests.

.PHONY: all _clean doc install test uninstall
