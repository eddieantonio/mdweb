#!/usr/bin/python

from __future__ import print_function
import timeit

TESTS_SETUP = """
import mdweb
FILES = {files}
referencable_chunks, parsed_chunks = mdweb.parse_input(FILES)
"""

TESTS = {
    'weaving': 'mdweb.weave(parsed_chunks)',
    'tangling': "mdweb.tangle(referencable_chunks, 'mdweb.py')",
    'parsing': "c, ac = mdweb.parse_input('mdweb-20120713.nw')"
}

FILES = [
    'mdweb-20120713.nw',
]

SAMPLES = 5
REPEAT = 1000

avg = lambda l: sum(l)/len(l)

if __name__ == '__main__':
    for test_name, test in TESTS.items():
        print("Test ", test_name)
        t = timeit.Timer(test, TESTS_SETUP.format(files=repr(FILES)))
        stats = t.repeat(SAMPLES, REPEAT)
        print(stats, "Avg ", avg(stats), '\n')


