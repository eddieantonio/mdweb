#!/usr/bin/env python

"""
A literate programming tool.

As a script, mdweb works a bit like noweave and notangle mushed into one,
with the weaver producing Markdown (as opposed to TeX) output.

mdweb can also be imported and extended as a module.  Provided are a
parser for noweb-like syntax of literate programs, as well as a flexible
documentation extractor (weaver) and a code extractor (tangler). These
can be extended, as the parser, its states, the tangler and the weaving
formatters are all classes.

"""

import re
import fileinput
import sys
import optparse
import collections
import itertools


DEFAULT_CHUNK_NAME = '*'

EXIT_TANGLE_ERROR = 1

COMMAND_LINE_OPTIONS = [
    optparse.make_option('-R', '--extract-chunk',
         dest='chunk_name', action='store',
         help='extract the chunk named CHUNK', metavar='CHUNK'),
    optparse.make_option('-w', '--weave',
         dest='weave',
         action='store_true',
         default=True,
         help='[default] extract documentation'),
    optparse.make_option('-t', '--tangle',
         dest='weave',
         action='store_false',
         help='extract a code chunk specified with `-R` option'),
    optparse.make_option('--encoding',
         dest='encoding',
         default='utf8',
         help="input and output encoding [defaults to 'utf8']")
]

USAGE = """\t%prog [options] [files]
\t%prog -R CHUNK [options] [files]"""
VERSION = "%prog 0.2"

code_chunk_start = re.compile(r'^<<([^>]+)>>\+?=\s*$')
code_chunk_end = re.compile(r'^@\s*$')
chunk_reference = re.compile("^(\s*)<<([^>]+)>>\s*")


class InfiniteExpansionError(ValueError):
    """
    Exception raised during tangling. Should be raised when a chunk
    or any of its referenced descendants contains a reference to itself,
    thereby creating an infinite expansion.
    """
    pass


def weave(all_chunks, formatter=None):
    """For each parsed chunk (that is, each item is a tuple containing
    (tag, attributes, lines) in all_chunks, formats it using formatter.
    Returns a generator, which itself will return each formatted chunk
    as a string.
    """
    
    # Use the MarkdownFormatter as the default.
    if formatter is None:
        formatter = MarkdownFormatter()

    for tag, attributes, lines in all_chunks:
        if tag == 'code':
            yield formatter.format_code(lines, **attributes)
        elif tag == 'docs':
            yield formatter.format_docs(lines)

def parse_input(files, encoding='utf8'):
    """
    Parses lines of input from either the list of files, or if the list
    is empty, resorts to standard input.

    Returns a tuple of (all_chunks, code_chunks), where all_chunks can
    be used as the first argument to weave() and code_chunks can be used
    as the first argument to tangle().
    """
    lines = fileinput.input(files,
        openhook=fileinput.hook_encoded(encoding))

    parser = Parser()
    return parser.parse(lines)

def tangle(chunks, requested_chunk=DEFAULT_CHUNK_NAME, indent=''):
    """
    Tangles (expands) the requested_chunk from a mapping of chunks.

    Returns the tangled chunk as a list of strings, each being a line
    from the expanded chunk.
    """
    tangler = Tangler(chunks)
    
    return tangler.expand_chunk(requested_chunk, indent)

def complain(message):
    """
    Print an error message to `stderr` in the defacto standard
    "program name: error" format.
    """
    program_name = sys.argv[0]
    sys.stderr.write("%s: %s\n" % (program_name, message))


class Parser(object):
    """
    A stateful mdweb parser.
    """
    def __init__(self):
        """
        Initializes a new mdweb parser. Parsing starts when an
        instance's `.parse(lines)` method is called.
        """
        self.all_chunks = collections.deque()
        self.lines = collections.deque()
        self.chunks = collections.defaultdict(collections.deque)

        self.current_chunk = None

        self.code_state = CodeState(self)
        self.doc_state = DocumentationState(self)

        # Default state is 'just typing'.
        self.state = self.doc_state
    
    def toggle_state(self, transition_details=None):
        """
        Toggles the state of the mdweb parser.  That is, if it is in
        'code' mode, it will toggle to 'documentation' mode and
        vice-versa. `transition_details` is any data needed to continue
        parsing.
        """
        self.state.commit_chunk()
        self.state.toggle_state(transition_details)
        self.lines = collections.deque()

    def parse(self, lines):
        """
        Given lines of mdweb source, this will start parsing them.
        Returns a tuple consisting of a dictionary of code chunks
        (indexed by its lowercase transformed description), and a tagged
        list of every chunk parsed (both code and documentation).
        """
        for line in lines:
            self.state.parse_line(line)

        # Final commit, or else the last chunk will vanish!
        self.state.commit_chunk()

        # If we return a defaultdict, it will mask KeyErrors.
        # So, convert it back into a normal dict.
        return dict(self.chunks), self.all_chunks

class ParserState(object):
    """
    Base class for Parser states. Provides the initializer so you don't
    have to!
    """
    def __init__(self, parser):
        """
        Initializes a Parser state with a reference to its parent.
        """
        self.parser = parser

class DocumentationState(ParserState):
    """
    State in which the parser is processing documentation (or "just
    text") chunks.
    """
    def parse_line(self, line):
        """
        Parses one line of input, toggling parent's parser state
        when a code chunk is detected.
        """
        code_started = code_chunk_start.match(line)
        if code_started:
            (chunk_name,) = code_started.groups()
            self.parser.toggle_state(chunk_name)
        else:
            self.parser.lines.append(line)

    def toggle_state(self, chunk_name):
        """
        Toggles the parent parser's state into `CodeState`.
        The parameter for this transition is simply the code chunk's name.
        """
        self.parser.current_chunk = chunk_name
        self.parser.current_chunk_indexable = chunk_name.lower()
        self.parser.state = self.parser.code_state
    

    def commit_chunk(self):
        """
        Commits the current documentation chunk for the parent parser's
        records. The current state's commit_chunk() should be called
        when before toggling states or after parsing has finished.
        """
        self.parser.all_chunks.append(
            ('docs', None, self.parser.lines))

class CodeState(ParserState):
    """
    State in which the parser is processing code chunks.
    """
    def parse_line(self, line):
        """
        Parses one line of a code chunk. Handles the transitions to
        documentation state when a single @ sign comprises the entire
        line.
        """
        if code_chunk_end.match(line):
            self.parser.toggle_state()
        else:
            index = self.parser.current_chunk_indexable
            self.parser.lines.append(line)
            self.parser.chunks[index].append(line)

    def toggle_state(self, *args):
        """
        Toggles parent parser's state into documentation mode. This
        state transition does not require any parameters.
        """
        self.parser.current_chunk = None
        self.parser.state = self.parser.doc_state

    def commit_chunk(self):
        """
        Commits the current code chunk to the parent parser's records.
        This should be called as the parser's state is being toggled and
        after parsing has been completed.
        """
        self.parser.all_chunks.append(
            ('code',
                {'name': self.parser.current_chunk,
                'index': self.parser.current_chunk_indexable},
                self.parser.lines))


class WeavingFormatter(object):
    """Weaving formatter base class. Currently offers no
    functionality."""
    pass

class MarkdownFormatter(WeavingFormatter):
    """Weaving formatter for Markdown. Also useful for plain-text."""
    CODE_INDENT = ' ' * 4

    def format_code_header(self, chunk_name):
        """
        Returns a code chunk header in string form.
        """
        return "#### %s =\n\n" % chunk_name
    
    def format_code(self, lines, **kwargs):
        """
        Returns a formatted code chunk (including its header) as a string.
        """
        header = self.format_code_header(kwargs['name'])
        indented_lines = (''.join((self.CODE_INDENT, line))
            if line.strip() else "\n" for line in lines)
    
        header_and_code = itertools.chain((header,), indented_lines)
    
        return ''.join(header_and_code)
    
    def format_docs(self, lines):
        """
        Returns a formatted documentation chunk as a string.
        """
        return ''.join(lines)


class Tangler(object):
    """
    Class for tangling -- that is, extracting code chunks and recursively
    expanding all chunks referenced within.

    Use expand_chunk() to expand whatever chunk needs expanding.
    """
    def __init__(self, chunks):
        """
        Initialize Tangler with a mapping of chunks that will be referenced
        when expanding chunks.
        """
        self.chunks = chunks
        self.expansion_stack = set()
    
    def expand_chunk(self, requested_chunk, indent=''):
        """
        Recursively expand a chunk. Returns the lines of the fully expanded
        chunk as a string. Raises an InfiniteExpansionError if a chunk
        contains a reference to itself (thereby trying to expand itself
        within itself).
        """
        chunk_index = requested_chunk.lower()
    
        if chunk_index in self.expansion_stack:
            raise InfiniteExpansionError(chunk_index)
        else:
            self.expansion_stack.add(chunk_index)
    
        expanded_chunk_lines = collections.deque()
        chunk_lines = self.chunks[chunk_index]
        
        for line in chunk_lines:
            reference = chunk_reference.match(line)
            if reference:
                more_indent, referenced_chunk = reference.groups()
                expanded_chunk_lines.extend(
                    self.expand_chunk(referenced_chunk, indent + more_indent))
            else:
                expanded_chunk_lines.append(''.join((indent, line)))
    
        self.expansion_stack.remove(chunk_index)
        return expanded_chunk_lines
    


def main(argv=None):
    """
    Parses command line arguments and acts accordingly, either weaving
    or tangling input, printing the result to standard output. Input can
    come from standard input or from the list of files specified on the
    command line.

    This function is intended to be called only when this module is run
    as a script. Much like a C main() function, this function returns 0
    on success, non-zero otherwise.
    """
    optparser = optparse.OptionParser(usage=USAGE, version = VERSION,
        option_list=COMMAND_LINE_OPTIONS)
    options, filenames = optparser.parse_args(args=argv)
    
    if options.chunk_name:
        options.weave = False
    elif not options.weave:
        options.chunk_name = DEFAULT_CHUNK_NAME

    code_chunks, parsed_lines = parse_input(filenames,
        encoding=options.encoding)

    if options.weave:
        output_lines = weave(parsed_lines)
    else:
        try:
            output_lines = tangle(code_chunks, options.chunk_name)
        except InfiniteExpansionError as error:
            (chunk_name,) = error.args
            complain("Inifinite expansion detected for '%s'" % chunk_name)
            return EXIT_TANGLE_ERROR
        except KeyError as error:
            (chunk_name,) = error.args
            complain("Could not find chunk named '%s'" % chunk_name)
            return EXIT_TANGLE_ERROR

    if options.encoding:
        import codecs
        output = codecs.getwriter(options.encoding)(sys.stdout)
    else:
        output = sys.stdout
    
    output.write(''.join(output_lines))

    return 0

if __name__ == '__main__':
    status = main()
    sys.exit(status)
