MDTANGLE = mdweb --tangle
MDWEAVE = mdweb --weave

.SUFFIXES: .nw .py .text
.nw.py:
	$(MDTANGLE) -R$@ $< > $@
.nw.text:
	$(MDWEAVE) $< > $@

